
#define dbg

char aux_str[100];
const char * __APN      = "safaricom";
const char * __usrnm    = "saf";
const char * __password = "data";



unsigned int Counter = 0;
unsigned long datalength, checksum, rLength;
unsigned short topiclength;
unsigned short topiclength2;
unsigned char topic[30];
char str[250];
unsigned char encodedByte;
int X;

unsigned short MQTTProtocolNameLength;
unsigned short MQTTClientIDLength;
unsigned short MQTTUsernameLength;
unsigned short MQTTPasswordLength;
unsigned short MQTTTopicLength;

const char * MQTTHost = "m14.cloudmqtt.com";
const char * MQTTPort = "17904";
const char * MQTTClientID = "ABCDEF";
const char * MQTTTopic = "SampleTopic";
const char * MQTTTopic2 = "SampleTopic2";
const char * MQTTProtocolName = "MQIsdp";
const char MQTTLVL = 0x03;
const char MQTTFlags = 0xC2;
const unsigned int MQTTKeepAlive = 60;
const char * MQTTUsername = "kfuhqble";
const char * MQTTPassword = "ouEFAawozlRf";
const char MQTTQOS = 0x00;
const char MQTTPacketID = 0x0001;

char sim800lreset = 4;

void setup()
{
  Serial.begin(115200);
  Serial2.begin(115200);

  pinMode(sim800lreset, OUTPUT);
  delay(3000);
  MQTTProtocolNameLength = strlen(MQTTProtocolName);



}

void loop()
{
  if (initTCP()) {
    MQTTConnect();
    readServerResponse("AT+CIPRXGET=2,1024", "OK", "ERROR", 10000);//receive data from n/w

    MQTTsubscribe();
    //readServerResponse("AT+CIPRXGET=2,1024", "OK", "ERROR", 10000);//receive data from n/w



    while (1) {
      MQTTpublish();
      readServerResponse("AT+CIPRXGET=2,1024", "OK", "ERROR", 10000);//receive data from n/w

      Counter++;
      delay(5000);
    }

  }
}

int  MQTTConnect() {

  if (sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000)) {
    Serial2.write(0x10);
    MQTTProtocolNameLength = strlen(MQTTProtocolName);
    MQTTClientIDLength = strlen(MQTTClientID);
    MQTTUsernameLength = strlen(MQTTUsername);
    MQTTPasswordLength = strlen(MQTTPassword);
    datalength = MQTTProtocolNameLength + 2 + 4 + MQTTClientIDLength + 2 + MQTTUsernameLength + 2 + MQTTPasswordLength + 2;
    X = datalength;
    do
    {
      encodedByte = X % 128;
      X = X / 128;
      // if there are more data to encode, set the top bit of this byte
      if ( X > 0 ) {
        encodedByte |= 128;
      }

      Serial2.write(encodedByte);
    }
    while ( X > 0 );
    Serial2.write(MQTTProtocolNameLength >> 8);
    Serial2.write(MQTTProtocolNameLength & 0xFF);
    Serial2.write(MQTTProtocolName);

    Serial2.write(MQTTLVL); // LVL
    Serial2.write(MQTTFlags); // Flags
    Serial2.write(MQTTKeepAlive >> 8);
    Serial2.write(MQTTKeepAlive & 0xFF);


    Serial2.write(MQTTClientIDLength >> 8);
    Serial2.write(MQTTClientIDLength & 0xFF);
    Serial2.print(MQTTClientID);


    Serial2.write(MQTTUsernameLength >> 8);
    Serial2.write(MQTTUsernameLength & 0xFF);
    Serial2.print(MQTTUsername);


    Serial2.write(MQTTPasswordLength >> 8);
    Serial2.write(MQTTPasswordLength & 0xFF);
    Serial2.print(MQTTPassword);

    Serial2.write(0x1A);
    if (sendATcommand2("", "SEND OK", "SEND FAIL", 5000)) {
      Serial.println(F("CONNECT PACKET SUCCESS"));
      return 1;
    }
    else return 0;
  }
}


int  MQTTpublish() {
  if (sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000)) {

    memset(str, 0, sizeof(str));

    topiclength = sprintf((char*)topic, MQTTTopic);
    //datalength = sprintf((char*)str,"%s%u",topic,Counter);
    datalength = sprintf((char*)str, "%s%s", topic, "Counter$$%%^&&&&**&^%$#");

    delay(1000);
    Serial2.write(0x30);
    X = datalength + 2;
    do
    {
      encodedByte = X % 128;
      X = X / 128;
      // if there are more data to encode, set the top bit of this byte
      if ( X > 0 ) {
        encodedByte |= 128;
      }
      Serial2.write(encodedByte);
    }
    while ( X > 0 );

    Serial2.write(topiclength >> 8);
    Serial2.write(topiclength & 0xFF);
    Serial2.print(str);
    Serial2.write(0x1A);
    if (sendATcommand2("", "SEND OK", "SEND FAIL", 5000)) {
      Serial.println(F("PUBLISH PACKET SENT"));
      return 1;
    }
    else return 0;
  }
}

void MQTTsubscribe() {

  if (sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000)) {

    memset(str, 0, 250);
    topiclength2 = strlen(MQTTTopic2);
    datalength = 2 + 2 + topiclength2 + 1;
    delay(1000);

    Serial2.write(0x82);
    X = datalength;
    do
    {
      encodedByte = X % 128;
      X = X / 128;
      // if there are more data to encode, set the top bit of this byte
      if ( X > 0 ) {
        encodedByte |= 128;
      }
      Serial2.write(encodedByte);
    }
    while ( X > 0 );
    Serial2.write(MQTTPacketID >> 8);
    Serial2.write(MQTTPacketID & 0xFF);
    Serial2.write(topiclength2 >> 8);
    Serial2.write(topiclength2 & 0xFF);
    Serial2.print(MQTTTopic2);
    Serial2.write(MQTTQOS);

    Serial2.write(0x1A);
    if (sendATcommand2("", "SEND OK", "SEND FAIL", 5000)) {
      Serial.println(F("SUBSCRIBE PACKET SENT"));
      return 1;
    }
    else return 0;
  }

}

